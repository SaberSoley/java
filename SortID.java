package Blatt1;
import java.util.Comparator;


/*Diese Klasse implementiert das Interface Comparator
 * Somit muss es die Methode compare beinhalten 
 * */
public class SortID implements Comparator<Customer> {
	
	/*Diese Methode bekommt zwei Customer Objecte und die ID's werden differenziert 
	 * Falls Gr��er dann 1, falls kleiner dann -1
	 */
	@Override
	public int compare (Customer c1, Customer c2){
		return c1.getCustomid() - c2.getCustomid();
	}

}

