package Blatt1;

public class Customer {
    //String
	String nachname;
	String vorname;
	static int id=0;
	int customid;
	//Constructor mit Vorname+Nachname Zähler wird automatisch hochgezählt
	public Customer(String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
		//zwischen speichern der id
		//customid=id;
		
		id++;
	} 
	public int getCustomid() {
		return customid;
	}
	public void setCustomid(int customid) {
		this.customid = customid;
	}
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	@Override
	public String toString() {
		return "Customer [nachname=" + nachname + ", vorname=" + vorname + "ID= "+ id + "]";
	}
	//Definition der equals methode, relevant für remove methode, wenn strings veglichen werden
    public boolean equals(String vorname2, String nachname2) {
   if ((vorname2 != vorname) &&(nachname2 != nachname)){
    return false;
   } else return true;
  }
	
	
	
	
	
	

}
