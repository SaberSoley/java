package Blatt1;
import java.util.Comparator;
/*Diese Klasse implementiert das Interface Comparator
 * Somit muss es die Methode compare beinhalten 
 * */
public class SortName implements Comparator<Customer> {
	/*Diese Methode bekommt zwei Customer Objecte und vergleicht die Nachname Strings mit compareto
	 * Falls Gr��er (lexikographisch) dann 1, falls kleiner dann -1, falls sie identisch sind dann 0
	 * Wenn sie identisch sind dann werden die Vornamen mit compare to verglichen */
	@Override
	public int compare (Customer c1, Customer c2){
		 int result=c1.getNachname().compareTo(c2.getNachname());
		
		if (result == 0){
		      result = c1.getVorname().compareTo(c2.getVorname());
		    }
		    if(result >= 1){
		      result = 1;
		    } else if(result <= -1){
		      result = -1;
		      }
		    return result;
	}

}
