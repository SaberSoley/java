package Blatt1;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Test;

public class MehtodenJUnit {

	@Test
	public void testSortByID() {
		Webshop c = new Webshop();
		Webshop d = new Webshop();
		c.addCustomer("Jogi", "L�we");
		c.addCustomer("Saber", "Solimany");
		c.addCustomer("Reza", "Hallo");
		d.addCustomer("Jogi", "L�we");
		d.addCustomer("Saber", "Solimany");
		d.addCustomer("Reza", "Hallo");
		Collections.sort(c.kunde, new SortID());
		Collections.sort(d.kunde, new SortID());
        assertEquals(c.kunde,d.kunde);
		
	}
	
	@Test
	public void testSortByNachname() {
		Webshop c = new Webshop();
		Webshop d = new Webshop();
		c.addCustomer("Jogi", "L�we");
		c.addCustomer("Saber", "Solimany");
		c.addCustomer("Reza", "Hallo");
		d.addCustomer("Jogi", "L�we");
		d.addCustomer("Saber", "Solimany");
		d.addCustomer("Reza", "Hallo");
		Collections.sort(c.kunde, new SortName());
		Collections.sort(d.kunde, new SortName());
        assertEquals(c,d);
		
	}

}
