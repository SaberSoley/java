package Blatt1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;

public class Webshop  {
    //Erstellen der Arraylist von Typ Customer
	List<Customer> kunde = new ArrayList();
	//Add Mthode bekommt zwei Strings, erstellt ein Customer Object und �ber gibt die Strings dem Constructor und das Customer Object wird in die Arraylist gepackt 
	public void addCustomer (String vorname, String nachname){
		Customer c = new Customer(vorname, nachname);
		kunde.add(c);
	}
	/*remove Methode bekommt zwei Strings, erstellt ein Customer Object mit den Strings (vorl�ufiges abspeichern)
	 * eine Schleige durchl�uft die einzelnen Indexe der Arraylist und vergleicht bei jedem Eintrag
	 *  Index die Vorname und Nachname Strings von c (vorl�ufiges Object)  */
	public void removeCustomer (String vorname, String nachname){
		Customer c = new Customer(vorname, nachname);
		for (int i=0; i<=kunde.size()-1; i++){
			if (kunde.get(i).getNachname().equals(c.getNachname())&&
					kunde.get(i).getVorname().equals(c.getVorname())){
				
				kunde.remove(i);
				
			}
		}
	}
	/*Diese Methode bekommt als Parameter ein Enum (SortbyID, SortbyNachname)
	 * Falls es sich um SortbyID Enum handelt, dann wird die Methode aufgerufen, die die Arraylist nach ID's sortiert
	 * Falls es um SortbyNachname handelt, dann wird die Methode aufgerufen, die die Arraylist nach den Nachnamen sortiert*/
	public void printlist(SortBy h ){
		
		if (h == SortBy.sortbyID){
			Collections.sort(kunde, new SortID());
			
		} else {
			
			Collections.sort(kunde,new SortName());
		}
		//Ausgabe jeden einnzelnen Customers mit durchlauf einer Schleife
		for (int i=0; i<=kunde.size()-1; i++){
			System.out.print(kunde.get(i).getVorname());
			System.out.print(",  " +kunde.get(i).getNachname());
			System.out.println(" (" + kunde.get(i).customid + ")");
		}
	}
	public Customer getKunde(int index) {
		return kunde.get(index);
	}
	
	  public boolean equals(ArrayList<Customer> a) {
		  boolean d = false;
		  for (int l=0; l<=kunde.size()-1; l++) {
		   if (a.get(l).equals(kunde.get(l).getVorname(), kunde.get(l).getNachname())) {
			   d= true;
		   } else 
		   d= false;
		  } return d;
		  }
			

	
	
	    
	    
	}


